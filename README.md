
## 平台简介

源计划MES系统移动端【开源版】，采用uniapp框架，一份代码多终端适配，同时支持APP、小程序、H5！

* 配套后台管理代码仓库地址[yjh-mes](https://gitee.com/Jintiago/yjh-mes)。
* 应用框架基于[uniapp](https://uniapp.dcloud.net.cn/)，支持小程序、H5、Android和IOS。
* 前端组件采用[uni-ui](https://github.com/dcloudio/uni-ui)，全端兼容的高性能UI框架。
* 特别鸣谢：[ruoyi-vue](https://gitee.com/y_project/RuoYi-Vue) ，[RuoYi-App](https://gitee.com/y_project/RuoYi-App) 。


## 技术文档

- 文档地址：待更新，可以先参考[若依框架](http://doc.ruoyi.vip/ruoyi-app/)。
- 微信交流： jinzhong373


## 演示图

<img src="./doc/screenshot/ydIndex.jpg" width="30%">     <img src="./doc/screenshot/yidongwork.jpg" width="30%">     <img src="./doc/screenshot/ydgongdan.jpg" width="30%">  

<img src="./doc/screenshot/ydbaogong.jpg" width="30%">     <img src="./doc/screenshot/ydlingliao.jpg" width="30%">     <img src="./doc/screenshot/ydweixiu.jpg" width="30%">  

<img src="./doc/screenshot/ydzhijian.jpg" width="30%"> 

## 🤝 商业服务

提供商业服务，团队做过几十家生产制造企业的ERP,WMS,MES系统。

有丰富的硬件数据采集、第三方系统集成(金蝶/用友等)经验。

可以微信联系【**jinzhong373**】。
