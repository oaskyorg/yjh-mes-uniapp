// 应用全局配置
module.exports = {
  
  //baseUrl: 'http://192.168.1.4:8080',
  baseUrl: 'http://127.0.0.1:8080',
  // 应用信息
  appInfo: {
    // 应用名称
    name: "yjh-mes-app",
    // 应用版本
    version: "1.0",
    // 应用logo
    logo: "/static/logo.png",
    // 官方网站
    site_url: "http://mes.sourceplan.cn",
    // 政策协议
    agreements: [{
        title: "隐私政策",
        url: "http://mes.sourceplan.cn"
      },
      {
        title: "用户服务协议",
        url: "http://mes.sourceplan.cn"
      }
    ]
  }
}
