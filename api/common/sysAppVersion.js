import request from '@/utils/request'

// 查询用户工位绑定列表
export function queryAppVersion() {
  return request({
    url: '/common/appVersion/query',
    method: 'get',
  })
}

