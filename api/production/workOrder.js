import upload from '@/utils/upload'
import request from '@/utils/request'

// 查询工单列表
export function listWorkOrder(query) {
  return request({
    url: '/production/workOrder/list',
    method: 'get',
    params: query
  })
}

// 查询工单详细
export function getWorkOrder(id) {
  return request({
    url: '/production/workOrder/' + id,
    method: 'get'
  })
}

// 查询工单详细(移动端)
export function listForMobile(query) {
  return request({
    url: '/production/workOrder/listForMobile' ,
	method: 'get',
    params: query
  })
}

//根据工单子表ID获取工单详细信息
export function getInfoByEntryId(id) {
  return request({
    url: '/production/workOrder/getInfoByEntryId/'+id ,
	method: 'get',
  })
}