import store from '@/store'
import {  getStation} from "@/api/masterdata/station";
import { mobileBindUserStation } from "@/api/common/sysUserStation";

/**
 * 扫码绑定工位
 */
export function scanCodeBindStation(_this) {
  uni.scanCode({
	onlyFromCamera: true,
	success: function (res) {
		console.log('条码类型：' + res.scanType);
		console.log('条码内容：' + res.result);
		//根据ID查询对应的工位
		getStation(res.result).then(response => {
			let data =response.data;
			//弹框是否确认绑定某某工位
			uni.showModal({
				title: "确认要绑定："+data.name+"吗?",
				// 取消按钮的文字自定义
				cancelText: "取消",
				// 确认按钮的文字自定义
				confirmText: "确认",
				//删除字体的颜色
				confirmColor:'red',
				//取消字体的颜色
				cancelColor:'#000000',
				success: function(res) {
					if (res.confirm) {
						let stationTmp ={};
						stationTmp.stationId =data.id;
						stationTmp.stationName = data.name
						//确认绑定,调用用户工位绑定新增接口
						mobileBindUserStation(stationTmp).then(resB => {
							_this.bindStationName=data.name;
							_this.bindWorkshopName=data.workshopName;
						})
						
					} 
				}
			})
			
		});
	}
  })
}

